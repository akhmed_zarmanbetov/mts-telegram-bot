from itertools import chain
from typing import List
import sqlite3

from mts_telebot import config as cfg
from mts_telebot import exceptions as ex


class Planner:
    def __init__(self, db_name: str = cfg.DB_NAME) -> None:
        self.db_name = db_name
        self._create_table()

    def _create_table(self):
        with sqlite3.connect(self.db_name) as conn:
            query = """
                CREATE TABLE IF NOT EXISTS planner(
                userid INT,
                plan TEXT
                );
            """
            conn.execute(query)

    def add(self, message):
        with sqlite3.connect(self.db_name) as conn:
            conn.execute(
                'INSERT INTO planner VALUES (?, ?)',
                (message.from_user.id, message.text)
            )

    def get_tasks(self, message) -> List[str]:
        with sqlite3.connect(self.db_name) as conn:
            cur = conn.cursor()
            cur.execute(
                'SELECT plan FROM planner WHERE userid==?', 
                (message.from_user.id,),
            )
            tasks = cur.fetchall()
            tasks = list(chain.from_iterable(tasks))

            if len(tasks) == 0:
                raise ex.TaskNotExists

            return tasks

    def show(self, message):
        tasks = self.get_tasks(message=message)

        prepare = self.prepare_tasks(tasks=tasks)

        return prepare

    def delete(self, message):
        with sqlite3.connect(self.db_name) as con:
            cursor = con.cursor()
            cursor.execute(
                'DELETE FROM planner WHERE userid==? AND plan==?', 
                (message.from_user.id, message.text),
            )

    def delete_all(self, message):
        with sqlite3.connect(self.db_name) as con:
            cursor = con.cursor()
            cursor.execute(
                'DELETE FROM planner WHERE userid==?',
                (message.from_user.id,),
            )
            con.commit()

    @staticmethod
    def prepare_tasks(tasks):
        prepare = "\n".join([str(i + 1) + ". " + task for i, task in enumerate(tasks)])
        return prepare