import os


TOKEN = os.environ['MTS_TELEBOT_TOKEN']
DB_NAME = 'planner.db'
ADD = 'Добавить дело в список'
SHOW = 'Показать список дел'
DELETE = 'Удалить дело из списка'
DELETE_ALL = "Удалить все дела из списка"
