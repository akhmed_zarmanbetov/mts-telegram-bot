import telebot
from telebot import types

from mts_telebot import config as cfg
from mts_telebot import exceptions as ex
from mts_telebot.database import Planner


print(cfg.TOKEN)
bot = telebot.TeleBot(cfg.TOKEN)
planner = Planner()


@bot.message_handler(commands=['start'])
def send_keyboard(message, text="Привет, чем я могу тебе помочь?"):
    keyboard = types.ReplyKeyboardMarkup(row_width=2)
    add_button = types.KeyboardButton(cfg.ADD)
    show_button = types.KeyboardButton(cfg.SHOW)
    delete_button = types.KeyboardButton(cfg.DELETE)
    delete_all_button = types.KeyboardButton(cfg.DELETE_ALL)
    keyboard.add(add_button, show_button)
    keyboard.add(delete_button, delete_all_button)

    message = bot.send_message(
        message.from_user.id,
        text=text, 
        reply_markup=keyboard,
    )

    bot.register_next_step_handler(message, callback_worker)


def callback_worker(message):
    if message.text == cfg.ADD:
        message = bot.send_message(message.chat.id, 'Давайте добавим дело! Напишите его в чат')
        bot.register_next_step_handler(message, add)

    elif message.text == cfg.SHOW:
        show(message=message)
    
    elif message.text == cfg.DELETE:
        try:
            tasks = planner.get_tasks(message=message)
            markup = types.ReplyKeyboardMarkup(row_width=2)
            for task in tasks:
                task_button = types.KeyboardButton(task)
                markup.add(task_button)

            message = bot.send_message(
                message.from_user.id,
                text = "Выбери одно дело из списка:",
                reply_markup=markup,
            )

            bot.register_next_step_handler(message, delete)
        except ex.TaskNotExists:
            bot.send_message(message.chat.id, "У тебя еще нет задач!")
            send_keyboard(message=message, text="Чем еще могу помочь?")

    elif message.text == cfg.DELETE_ALL:
        delete_all(message=message)


@bot.message_handler(content_types=['text'])
def handle_docs_audio(message):
    send_keyboard(message, text="Я не понимаю. Выберите один из пунктов меню:")


def add(message):
    planner.add(message=message)
    bot.send_message(message.chat.id, "Дело добавлено!")
    send_keyboard(message=message, text="Чем еще могу помочь?")


def show(message):
    try:
        tasks = planner.show(message=message)
        bot.send_message(message.chat.id, tasks)
    except ex.TaskNotExists:
        bot.send_message(message.chat.id, "У тебя еще нет задач!")
    finally:
        send_keyboard(message=message, text="Чем еще могу помочь?")


def delete(message):
    planner.delete(message=message)
    bot.send_message(message.chat.id, f'Задача: "{message.text}" удалена.')
    send_keyboard(message, "Чем еще могу помочь?")


def delete_all(message):
    planner.delete_all(message=message)
    bot.send_message(message.chat.id, 'Удалены все дела!')
    send_keyboard(message, "Чем еще могу помочь?")


if __name__ == '__main__':
     bot.infinity_polling()
